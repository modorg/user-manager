# frozen_string_literal: true
Rails.application.routes.draw do
  # devise_for :users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # Easier to make GET request from the client
  devise_for :users, sign_out_via: [:get, :delete], controllers: {
    omniauth_callbacks: 'callbacks', registrations: 'registrations', sessions: 'sessions'
  }

  root 'home#index'

  devise_scope :user do
    get 'registered_successfully', to: 'registrations#registered_successfull',
                                   as: :successfull_registration
  end

  # Provider stuff
  match '/auth/sso/authorize' => 'auth#authorize', via: :all
  match '/auth/sso/access_token' => 'auth#access_token', via: :all
  match '/auth/sso/user' => 'auth#user', via: :all
  match '/oauth/token' => 'auth#access_token', via: :all
end
