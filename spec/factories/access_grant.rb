# frozen_string_literal: true

require 'rails_helper'

FactoryGirl.define do
  factory :access_grant do
    code                     'codeXXX'
    access_token             'access_tokenXXX'
    refresh_token            'refresh_tokenXXX'
    access_token_expires_at  1.day.from_now
    association              :user
    association              :client
    state                    'stateXXX'
    sso_session              SecureRandom.hex
  end
end
