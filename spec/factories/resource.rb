# frozen_string_literal: true

require 'rails_helper'

FactoryGirl.define do
  factory :resource do
    client
    sequence(:keyword) { |n| "committee#{n}" }
  end
end
