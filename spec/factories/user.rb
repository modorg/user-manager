# frozen_string_literal: true

require 'rails_helper'

FactoryGirl.define do
  factory :user do
    sequence :email do |n|
      "person#{n}@example.com"
    end
    password 'samplepass'
  end
end
