# frozen_string_literal: true

require 'rails_helper'

FactoryGirl.define do
  factory :client do
    sequence(:name) { |n| "app#{n}" }
    app_id         'app_id'
    app_secret     'app_secret'
  end
end
