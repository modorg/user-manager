# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Client, type: :model do
  let(:client) { create(:client) }

  describe '.authenticate' do
    it 'calls proper function' do
      expect(Client).to receive(:find_by).with([
                                                 'app_id = ? AND app_secret = ?',
                                                 client.app_id,
                                                 client.app_secret
                                               ])
      Client.authenticate(client.app_id, client.app_secret)
    end

    it 'returns proper value' do
      expect(Client.authenticate(client.app_id, client.app_secret)).to eq(client)
    end
  end
end
