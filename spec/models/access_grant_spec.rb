# frozen_string_literal: true
require 'rails_helper'

RSpec.describe AccessGrant, type: :model do
  it { is_expected.to validate_presence_of :sso_session }

  let(:access_grant) { build(:access_grant) }

  it { is_expected.to belong_to :user }
  it { is_expected.to belong_to :client }

  describe '#generate_tokens' do
    it 'should receive generate_tokens after create' do
      expect(access_grant).to receive(:generate_tokens)
      access_grant.save
    end

    it 'should call secure random' do
      expect(SecureRandom).to receive(:hex).exactly(3).times.with(16)
      access_grant.generate_tokens
    end

    context 'setting fields' do
      before(:each) { access_grant.generate_tokens }
      it { expect(access_grant.code).to be }
      it { expect(access_grant.access_token).to be }
      it { expect(access_grant.refresh_token).to be }
    end
  end

  describe '.prune!' do
    it do
      expect(AccessGrant).to receive_message_chain(:where, :delete_all)
      AccessGrant.prune!
    end
  end

  describe '#redirect_uri_for' do
    it 'returns proper value for uri with question mark' do
      expect(access_grant.redirect_uri_for('sample.com?a=b')).to include('&',
                                                                         'code', 'response_type',
                                                                         'state', access_grant.code,
                                                                         access_grant.state)
    end

    it 'returns proper value for uri without question mark' do
      expect(access_grant.redirect_uri_for('sample.com?a=b')).to include('?',
                                                                         'code', 'response_type',
                                                                         'state', access_grant.code,
                                                                         access_grant.state)
    end
  end

  describe '.authenticate' do
    it 'calls find_by method with proper params' do
      expect(AccessGrant).to receive(:find_by).with('code = ? AND client_id = ?', 'code', 'app')
      AccessGrant.authenticate('code', 'app')
    end

    it 'returns proper value' do
      access_grant.save!
      expect(
        AccessGrant.authenticate(access_grant.code, access_grant.client_id)
      ).to eq(access_grant)
    end
  end

  describe '#start_expiry_period!' do
    it 'calls proper methos' do
      expect(access_grant).to receive(:update_attributes)
        .with(access_token_expires_at: kind_of(Time))
      access_grant.start_expiry_period!
    end
  end
end
