# frozen_string_literal: true
require 'rails_helper'

RSpec.describe UsersPermission, type: :model do
  it { is_expected.to validate_presence_of(:permitted_id) }

  describe 'user_resouce_permitted_id_uniq_contraint' do
    subject do
      described_class.new(user: create(:user), resource: create(:resource), permitted_id: 1)
    end

    it { is_expected.to validate_uniqueness_of(:permitted_id).scoped_to([:user_id, :resource_id]) }
  end

  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:resource) }
end
