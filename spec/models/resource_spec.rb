# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Resource, type: :model do
  it { is_expected.to validate_presence_of :client }
  it { is_expected.to validate_presence_of :keyword }

  describe 'keyword client uniqueness' do
    subject { described_class.new(keyword: 'sample', client: create(:client)) }
    it { is_expected.to validate_uniqueness_of(:keyword).scoped_to(:client_id) }
  end

  it { is_expected.to belong_to :client }
end
