# frozen_string_literal: true
require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { create(:user) }
  let(:auth) { OpenStruct.new(provider: nil, uid: user.uid) }

  it { is_expected.to have_many(:access_grants).dependent(:delete_all) }
  it { is_expected.to have_many(:users_permissions) }

  describe '.from_omniauth' do
    it 'calls where method' do
      expect(User).to receive(:where).and_return(User.all)
      User.from_omniauth(auth)
    end
  end

  describe '#permissions' do
    let(:client1) { create(:client) }
    let(:client2) { create(:client) }
    let(:resource1) { create(:resource, client: client1) }
    let(:resource2) { create(:resource, client: client1) }
    let(:resource3) { create(:resource, client: client2) }

    subject { user.permissions }
    it { is_expected.to eq({}) }

    context 'user with permissions' do
      before { user.users_permissions.create!(resource: resource1, permitted_id: 8) }

      it { expect(subject.keys).to eq([client1.name]) }
      it { expect(subject.values).to eq([{ resource1.keyword => ['8'] }]) }

      context 'added second organization from first app' do
        before { user.users_permissions.create!(resource: resource1, permitted_id: 35) }

        it { expect(subject.keys).to eq([client1.name]) }
        it { expect(subject.values).to eq([{ resource1.keyword => %w(8 35) }]) }

        context 'added second resource' do
          before { user.users_permissions.create!(resource: resource2, permitted_id: '99') }

          it { expect(subject.keys).to eq([client1.name]) }
          it do
            expect(subject.values).to eq([{ resource1.keyword => %w(8 35),
                                            resource2.keyword => ['99'] }])
          end

          context 'added another apps resource' do
            before { user.users_permissions.create!(resource: resource3, permitted_id: '303') }

            it { expect(subject.keys).to eq([client1, client2].map(&:name)) }

            it 'has proper final values' do
              expect(subject.values).to eq([{ resource1.keyword => %w(8 35),
                                              resource2.keyword => ['99'] },
                                            { resource3.keyword => ['303'] }])
            end
          end
        end
      end
    end
  end
end
