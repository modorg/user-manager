class AddAppUrlToClient < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :app_url, :string
  end
end
