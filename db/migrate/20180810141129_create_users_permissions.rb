class CreateUsersPermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :users_permissions do |t|
      t.references :user, foreign_key: true, null: false
      t.references :resource, foreign_key: true, null: false
      t.integer :permitted_id

      t.timestamps
    end

    add_index :users_permissions, [:user_id, :resource_id, :permitted_id], unique: true,
                                  name: 'user_resouce_permitted_id_uniq_contraint'
  end
end
