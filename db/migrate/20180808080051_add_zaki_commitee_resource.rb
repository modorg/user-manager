class AddZakiCommiteeResource < ActiveRecord::Migration[5.0]
  def up
    Resource.create(client_id: Client.find_by(name: 'zaki')&.id, keyword: 'committee')
  end

  def down
    Resource.all.delete_all
  end
end
