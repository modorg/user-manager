class AddSsoSessionToAccessGrant < ActiveRecord::Migration[5.0]
  def change
    add_column :access_grants, :sso_session, :string
  end
end
