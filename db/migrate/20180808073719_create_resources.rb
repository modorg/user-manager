class CreateResources < ActiveRecord::Migration[5.0]
  def change
    create_table :resources do |t|
      t.references :client, foreign_key: true, null: false
      t.string :keyword

      t.timestamps
    end

    add_index :resources, [:client_id, :keyword], unique: true
  end
end
