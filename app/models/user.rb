# frozen_string_literal: true
class User < ApplicationRecord
  # t.string   "email",                  default: "",    null: false
  # t.string   "encrypted_password",     default: "",    null: false
  # t.string   "reset_password_token"
  # t.datetime "reset_password_sent_at"
  # t.datetime "remember_created_at"
  # t.integer  "sign_in_count",          default: 0,     null: false
  # t.datetime "current_sign_in_at"
  # t.datetime "last_sign_in_at"
  # t.inet     "current_sign_in_ip"
  # t.inet     "last_sign_in_ip"
  # t.datetime "created_at",                             null: false
  # t.datetime "updated_at",                             null: false
  # t.string   "provider"
  # t.string   "uid"
  # t.boolean  "is_admin",               default: false, null: false
  # t.string   "confirmation_token"
  # t.datetime "confirmed_at"
  # t.datetime "confirmation_sent_at"
  # t.boolean  "approved",               default: false, null: false
  # t.index ["approved"], name: "index_users_on_approved", using: :btree
  # t.index ["confirmation_token"], name: "index_users_on_confirmation_token",
  # unique: true, using: :btree
  # t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
  # t.index ["reset_password_token"], name: "index_users_on_reset_password_token",
  # unique: true, using: :btree

  # relations
  has_many :access_grants, dependent: :delete_all
  has_many :users_permissions

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :confirmable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: [:facebook]

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
    end
  end

  def active_for_authentication?
    super && approved?
  end

  def inactive_message
    if !approved?
      :not_approved
    else
      super
    end
  end

  def permissions
    p = permissions_grouped_by_clients
    p = permissions_key_parsed_value_grouped(p)
    p.each { |_, v| v.map { |_, vv| vv.replace vv.map(&:permitted_id).map(&:to_s) } }
  end

  def display_name
    email
  end

  private

  def permissions_key_parsed_value_grouped(p)
    p.each do |k, v|
      p[k] = v.group_by { |up| up.resource.keyword }
    end
  end

  def permissions_grouped_by_clients
    users_permissions.includes(resource: :client).group_by { |up| up.resource.client.name }
  end
end
