# frozen_string_literal: true
class UsersPermission < ApplicationRecord
  # t.integer  "user_id",      null: false
  # t.integer  "resource_id",  null: false
  # t.integer  "permitted_id"
  # t.datetime "created_at",   null: false
  # t.datetime "updated_at",   null: false
  # t.index ["resource_id"], name: "index_users_permissions_on_resource_id", using: :btree
  # t.index ["user_id", "resource_id", "permitted_id"],
  # name: "user_resouce_permitted_id_uniq_contraint", unique: true, using: :btree
  # t.index ["user_id"], name: "index_users_permissions_on_user_id", using: :btree

  # validations
  validates :permitted_id, presence: true
  validates :permitted_id, uniqueness: { scope: [:user_id, :resource_id] }

  # relations
  belongs_to :user
  belongs_to :resource
end
