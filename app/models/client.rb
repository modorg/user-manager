# frozen_string_literal: true
class Client < ApplicationRecord
  # t.string   "name"
  # t.string   "app_id"
  # t.string   "app_secret"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  # t.string   "app_url"

  # validations
  validates :name, :app_id, :app_secret, presence: true

  # Check whether a Client exists by app_id and app_secret
  def self.authenticate(app_id, app_secret)
    find_by(['app_id = ? AND app_secret = ?', app_id, app_secret])
  end
end
