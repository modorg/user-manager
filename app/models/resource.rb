# frozen_string_literal: true
class Resource < ApplicationRecord
  # t.integer  "client_id",  null: false
  # t.string   "keyword"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  # t.index ["client_id", "keyword"], name: "index_resources_on_client_id_and_keyword",
  # unique: true, using: :btree
  # t.index ["client_id"], name: "index_resources_on_client_id", using: :btree

  # validations
  validates :client, :keyword, presence: true
  validates :keyword, uniqueness: { scope: :client_id }

  # relations
  belongs_to :client

  def display_name
    "#{client.name} - #{keyword}"
  end
end
