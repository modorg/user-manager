# frozen_string_literal: true
class AccessGrant < ApplicationRecord
  # t.string   "code"
  # t.string   "access_token"
  # t.string   "refresh_token"
  # t.datetime "access_token_expires_at"
  # t.integer  "user_id"
  # t.integer  "client_id"
  # t.string   "state"
  # t.datetime "created_at",              null: false
  # t.datetime "updated_at",              null: false
  # t.string   "sso_session"

  # validations
  validates :sso_session, presence: true

  # relations
  belongs_to :user
  belongs_to :client

  # callbacks
  before_create :generate_tokens

  # Generate random tokens
  def generate_tokens
    self.code = SecureRandom.hex(16)
    self.access_token = SecureRandom.hex(16)
    self.refresh_token = SecureRandom.hex(16)
  end

  def self.prune!
    where(['created_at < ?', 3.days.ago]).delete_all
  end

  def redirect_uri_for(redirect_uri)
    if redirect_uri =~ /\?/
      redirect_uri + "&code=#{code}&response_type=code&state=#{state}"
    else
      redirect_uri + "?code=#{code}&response_type=code&state=#{state}"
    end
  end

  def self.authenticate(code, application_id)
    AccessGrant.find_by('code = ? AND client_id = ?', code, application_id)
  end

  def self.authenticate_and_refresh(refresh_token, application_id)
    AccessGrant.find_by('refresh_token = ? AND client_id = ?', refresh_token,
                        application_id)&.refresh!
  end

  def refresh!
    update_attributes(code: SecureRandom.hex(16), refresh_token: SecureRandom.hex(16))
    self
  end

  # Note: This is currently configured through devise
  # and matches the AuthController access token life
  def start_expiry_period!
    update_attributes(access_token_expires_at: (Time.zone.now + Devise.timeout_in))
  end
end
