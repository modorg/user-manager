# frozen_string_literal: true
module ApplicationHelper
  def controller_stylesheet_present?
    Rails.application.config.assets.precompile.include?("#{params[:controller]}.css")
  end
end
