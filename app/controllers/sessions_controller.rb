# frozen_string_literal: true
class SessionsController < Devise::SessionsController
  after_action :set_sso_session_cookie, only: :create
  before_action :delete_sso_session_cookie, only: :destroy

  private

  def set_sso_session_cookie
    cookies[:sso_session] = { value: SecureRandom.hex, domain: :all }
  end

  def delete_sso_session_cookie
    AccessGrant.where(sso_session: cookies[:sso_session]).delete_all
    cookies.delete(:sso_session, domain: :all)
  end
end
