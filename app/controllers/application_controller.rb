# frozen_string_literal: true
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def authenticate_admin_user!
    raise SecurityError unless current_user&.is_admin?
  end

  rescue_from SecurityError do |_exception|
    redirect_to root_url, alert: I18n.t(:only_admin)
  end
end
