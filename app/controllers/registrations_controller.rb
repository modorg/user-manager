# frozen_string_literal: true
class RegistrationsController < Devise::RegistrationsController
  def registered_successfull; end

  protected

  def after_inactive_sign_up_path_for(_resource)
    successfull_registration_path
  end
end
