# frozen_string_literal: true
class AuthController < ApplicationController
  # This is our new function that comes before Devise's one
  before_action :authenticate_user_from_token!, except: [:access_token]

  before_action :authenticate_user!, except: [:access_token]
  skip_before_action :verify_authenticity_token, only: [:access_token, :authorize]

  def authorize
    # Note: this method will be called when the user
    # is logged into the provider
    #
    # So we're essentially granting him access to our
    # system by generating certain tokens and then
    # redirecting him back to the params[:redirect_uri]
    # with a random code and the params[:state]

    AccessGrant.prune!
    create_hash = {
      client: application,
      state: params[:state],
      sso_session: cookies[:sso_session]
    }
    access_grant = current_user.access_grants.create(create_hash)
    redirect_to access_grant.redirect_uri_for(params[:redirect_uri])
  end

  # POST
  # rubocop:disable Metrics/MethodLength
  def access_token
    application = Client.authenticate(params[:client_id], params[:client_secret])

    if application.nil?
      render json: { error: 'Could not find application' }
      return
    end

    if params[:grant_type] == 'refresh_token'
      access_grant = AccessGrant.authenticate_and_refresh(params[:refresh_token], application.id)
    else
      access_grant = AccessGrant.authenticate(params[:code], application.id)
    end

    if access_grant.nil?
      render json: { error: 'Could not authenticate access code or refresh token' }
      return
    end

    access_grant.start_expiry_period!
    render json: { access_token: access_grant.access_token,
                   refresh_token: access_grant.refresh_token,
                   expires_in: Devise.timeout_in.to_i }
  end

  def user
    hash = {
      provider: 'sso',
      id: current_user.id.to_s,
      is_admin: current_user.is_admin?,
      permissions: current_user.permissions,
      info: {
        email: current_user.email
      },
      extra: {
        #  first_name: current_user.first_name,
        #  last_name: current_user.last_name
        first_name: '',
        last_name: ''
      }
    }

    render json: hash.to_json
  end
  # rubocop:enable Metrics/MethodLength

  protected

  def application
    @application ||= Client.find_by(app_id: params[:client_id])
  end

  private

  def authenticate_user_from_token!
    return unless params[:oauth_token]
    access_grant = AccessGrant.where(access_token: params[:oauth_token]).take
    sign_in access_grant.user if access_grant.user # Devise sign in
  end
end
