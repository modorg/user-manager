# frozen_string_literal: true
ActiveAdmin.register UsersPermission do
  actions :all, except: [:edit, :show, :update]

  form do |f|
    f.semantic_errors
    f.inputs do
      f.input :user
      f.input :resource
      f.input :permitted_id
    end
    f.actions
  end

  permit_params :user_id, :resource_id, :permitted_id

  controller do
    def scoped_collection
      UsersPermission.includes({ resource: :client }, :user)
    end
  end
end
