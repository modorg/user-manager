# frozen_string_literal: true
ActiveAdmin.register Client do
  permit_params :name, :app_id, :app_secret, :app_url

  form do |f|
    f.inputs do
      f.input :name
      f.input :app_id
      f.input :app_secret
      f.input :app_url
    end
    f.actions
  end
end
