# frozen_string_literal: true
ActiveAdmin.register AccessGrant do
  controller do
    actions :all, except: [:edit, :destroy]
  end

  index do
    selectable_column
    id_column
    column :access_token_expires_at
    column :user
    column :client
    column :sso_session
    column :created_at
    column :updated_at
    actions
  end
end
