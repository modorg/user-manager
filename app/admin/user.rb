# frozen_string_literal: true
ActiveAdmin.register User do
  permit_params :email, :password, :password_confirmation, :is_admin, :approved

  index do
    selectable_column
    id_column
    column :email
    column :approved
    column :current_sign_in_at
    column :sign_in_count
    column :is_admin
    column :created_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :approved
      f.input :is_admin
    end
    f.actions
  end

  sidebar I18n.t(:permitted_resources), only: :show do
    attributes_table_for user do
      user.permissions.each do |app, p|
        p.each do |resource, permitted_ids|
          row "#{app} - #{resource}" do
            permitted_ids.join(',')
          end
        end
      end
    end
    link_to t(:edit_permissions), { :controller => 'users_permissions', :action => 'index',
                                    'q[user_id_eq]' => user.id.to_s }, class: 'text-center d-block'
  end

  controller do
    def update
      if params[:user][:password].blank?
        %w(password password_confirmation).each { |p| params[:user].delete(p) }
      end
      super
    end
  end
end
